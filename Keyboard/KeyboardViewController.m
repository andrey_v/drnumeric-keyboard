//
//  KeyboardViewController.m
//  Keyboard
//
//  Created by Andrey Vasilev on 03.10.15.
//  Copyright (c) 2015 Andrey Vasilev. All rights reserved.
//

#import "KeyboardViewController.h"
#define kButtonsPerRow 3
#define kSpaceButtonTitle @"Пробел"

@interface KeyboardViewController ()
@property (nonatomic, strong) UIButton *nextKeyboardButton;
@property (nonatomic, strong) NSMutableArray *drNumButtonsArray;
@end

@implementation KeyboardViewController

- (void)updateViewConstraints {
    [super updateViewConstraints];
    
    // Add custom view sizing constraints here
    [self resizeButtons];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Perform custom UI setup here
    self.nextKeyboardButton = [UIButton buttonWithType:UIButtonTypeSystem];
    
    [self.nextKeyboardButton setTitle:NSLocalizedString(@"Next Keyboard", @"Title for 'Next Keyboard' button") forState:UIControlStateNormal];
    [self.nextKeyboardButton sizeToFit];
    self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.nextKeyboardButton addTarget:self action:@selector(advanceToNextInputMode) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.nextKeyboardButton];
    
    NSLayoutConstraint *nextKeyboardButtonLeftSideConstraint = [NSLayoutConstraint constraintWithItem:self.nextKeyboardButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0];
    NSLayoutConstraint *nextKeyboardButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.nextKeyboardButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [self.view addConstraints:@[nextKeyboardButtonLeftSideConstraint, nextKeyboardButtonBottomConstraint]];
    
    [self initButtons];
}


- (void)textWillChange:(id<UITextInput>)textInput {
    // The app is about to change the document's contents. Perform any preparation here.
}

- (void)textDidChange:(id<UITextInput>)textInput {
    // The app has just changed the document's contents, the document context has been updated.
    
    UIColor *textColor = nil;
    if (self.textDocumentProxy.keyboardAppearance == UIKeyboardAppearanceDark) {
        textColor = [UIColor whiteColor];
    } else {
        textColor = [UIColor blackColor];
    }
    [self.nextKeyboardButton setTitleColor:textColor forState:UIControlStateNormal];
}


- (void)initButtons {
    _drNumButtonsArray = [@[] mutableCopy];
    NSArray *drNumButtonsTitles = @[@[@"1",@"2",@"3"],
                                    @[@"4",@"5",@"6"],
                                    @[@"7",@"8",@"9"],
                                    @[@"D",@"0",@"R"],
                                    @[@".",kSpaceButtonTitle,@","]];
    [drNumButtonsTitles enumerateObjectsUsingBlock:^(NSArray *rowTitlesArray, NSUInteger idx, BOOL *stop) {
        __block NSMutableArray *rowArray = [@[] mutableCopy];
        [rowTitlesArray enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL *stop) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [button setTitle:title forState:UIControlStateNormal];
            [button addTarget:self action:@selector(simpleButton_pressed:) forControlEvents:UIControlEventTouchUpInside];
            [button setBackgroundColor:[UIColor whiteColor]];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.view addSubview:button];
            [rowArray addObject:button];
        }];
        [_drNumButtonsArray addObject:rowArray];
    }];
}

- (void)resizeButtons {
    
    CGFloat space = 2;
    CGFloat simpleButtonHeight = (self.view.frame.size.height-34-(space * (_drNumButtonsArray.count+1)))/_drNumButtonsArray.count;
    
    [_drNumButtonsArray enumerateObjectsUsingBlock:^(NSMutableArray *rowArray, NSUInteger rowIdx, BOOL *stop) {
        CGFloat simpleButtonWidth = (self.view.frame.size.width-(space * (kButtonsPerRow+1)))/kButtonsPerRow;

        [rowArray enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger columnIdx, BOOL *stop) {
            [button setFrame:CGRectMake(space + (simpleButtonWidth+space)*columnIdx, space + (simpleButtonHeight+space)*rowIdx, simpleButtonWidth, simpleButtonHeight)];
        }];
    }];
}

- (void)simpleButton_pressed:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:kSpaceButtonTitle])
        NSLog(@" ");
    else
        NSLog(@"%@", sender.titleLabel.text);
}

@end
