//
//  KeyboardViewController.h
//  Keyboard
//
//  Created by Andrey Vasilev on 03.10.15.
//  Copyright (c) 2015 Andrey Vasilev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardViewController : UIInputViewController

@end
